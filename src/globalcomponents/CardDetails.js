import React from 'react';
import {CardElement, injectStripe} from 'react-stripe-elements';
import Axios from 'axios';
import {Button} from 'reactstrap';

const CardDetails = (props) => {

	let property = props.property

	const handlePayAndBookClass = async (e) => {
        props.handleAddBooking()
		let token = await props.stripe.createToken({name:"name"});
        // console.log(token)
		let price = property.price*100
		let description = "Payment for booking";

		let response = await Axios.post('https://dry-inlet-35094.herokuapp.com/charge', {
			headers: {"Content-type":"text-plain"},
			body: token.id,
			amount: price,
			description: description
		})

		if(response.ok){
			console.log("Purchase Complete");
		}
	    
	}

	return (
		<div className="checkout">
			<p>Amount: {property.price} PHP</p>
        	<p>Would you like to complete the purchase?</p>
        	<CardElement />
        	<div className="d-flex justify-content-center my-3">
	        	<Button
	        		color="success"
	        		className="mx-1"
	        		onClick={handlePayAndBookClass}
	        		disabled={props.startDate==="" || props.endDate==="" ? true : false}
	        	>

	        		Book Now
	        	</Button>
	        	<Button
					color="danger"
					className="mx-1"
					onClick={props.handleCancelBooking}
				>
					Cancel
				</Button>
			</div>
      	</div>
	)
}

export default injectStripe(CardDetails);