import React from 'react';
import {Elements, StripeProvider} from 'react-stripe-elements';
import CardDetails from './CardDetails';

const Stripe = (props) => {
	return(
		<React.Fragment>
			<StripeProvider
				apiKey="pk_test_641k1hMSMvBZCJSdb2COFfw800MKGOVelD"
			>
			<div>
				<h3 className="text-center py-1">Payment Details</h3>
				<div className="d-flex justify-content-center">
					<Elements>
						<CardDetails 
							property={props.property}
							handleAddBooking={props.handleAddBooking}
							//handleCancelBooking={props.handleCancelBooking}
							startDate={props.startDate}
                            endDate={props.endDate}
						/>
					</Elements>
				</div>
			</div>
			</StripeProvider>
		</React.Fragment>
	)
}

export default Stripe;