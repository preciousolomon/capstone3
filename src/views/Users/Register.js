import React, {useState} from 'react';
import { FormInput } from '../../globalcomponents';
import { Button } from 'reactstrap';
import Axios from 'axios';
import { NavigationBar, Footer } from '../Layouts';
import { Link } from 'react-router-dom';



const Register = () => {
    const [firstName, setFirstName] = useState('');
    const [lastName, setLastName] = useState('');
    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');

    const handleFirstNameChange = (e) => {
        setFirstName(e.target.value)
        console.log(e.target.value);
    }
    const handleLastNameChange = (e) => {
        setLastName(e.target.value)
        console.log(e.target.value);
    }
    const handleEmailChange = (e) => {
        setEmail(e.target.value)
        console.log(e.target.value);
    }
    const handlePasswordChange = (e) => {
        setPassword(e.target.value)
        console.log(e.target.value);
    }

    const handleRegister = () => {
        Axios.post('https://dry-inlet-35094.herokuapp.com/register', {
            firstName:firstName,
            lastName:lastName,
            email:email,
            password:password
        }).then(res => console.log(res.data))
        window.location.replace('#/login');
    }
    return (
        <React.Fragment>
            <div>
            <NavigationBar />
            <div className="col-lg-4 offset-lg-4 border container-fluid">
            <h1 className="text-center text-strong text-dark pt-5">Register</h1>
           
                <FormInput
                    label={"First Name"}
                    placeholder={"Enter your first name"}
                    type={"text"}
                    onChange={handleFirstNameChange}
                />
                <FormInput
                    label={"Last Name"}
                    placeholder={"Enter your last name"}
                    type={"text"}
                    onChange={handleLastNameChange}
                />
                 <FormInput
                    label={"Email"}
                    placeholder={"Enter your email address"}
                    type={"email"}
                    onChange={handleEmailChange}
                />
                 <FormInput
                    label={"Password"}
                    placeholder={"Enter your password"}
                    type={"password"}
                    onChange={handlePasswordChange}
                />
                <Button
                    block
                    color="info"
                    onClick={handleRegister}
                >
                Register
                </Button>
                <p> <Link to="/login">Already a user?</Link></p>
                
            </div>
            
                <Footer />
                </div>
        </React.Fragment>
    )
}

export default Register;