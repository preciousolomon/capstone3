import React from 'react';
import {
	Button
	
} from 'reactstrap'



const BookList = props => {

	

	const booking = props.booking;
	const userData = props.userData;

	let userFirstName = "";
	let userLastName = "";

	userData.map(indivUserData => {
		if (indivUserData._id ===booking.userId) {
			userFirstName = indivUserData.firstName;
			userLastName = indivUserData.lastName;
		}
	})

	console.log(booking);

	

	return (
		<React.Fragment>
			<tr className="text-center">
				<td> {booking.bookingCode}</td>
				{props.user.isAdmin ==="true" ?
					<td>
						{userFirstName + " " + userLastName}</td>
				: ""}					
				<td> {booking.createdAt}</td>
				<td> {booking.startDate} </td>
				<td> {booking.endDate} </td>
				<td> {booking.roomName}</td>
				<td> {booking.days}</td>
				<td> PHP {booking.totalPayment}</td>
			
				<td>
					{props.user.isAdmin ==="true" ?
					<td>
						<Button
						color="danger"
						className="col-lg-12"
						onClick={()=>props.handleDeleteBooking(booking._id)}>
						Delete Booking
					</Button></td>
				: ""}	
					
				</td>
				<td>
				
				</td>			
			</tr>				
							
		</React.Fragment>
	)
}

export default BookList;