import React, {useState, useEffect} from 'react';
import { Link } from 'react-router-dom';
import {
    Collapse,
    Navbar,
    NavbarToggler,
    NavbarBrand
} from 'reactstrap';
import { logo} from './index';

const NavigationBar = () => {
    const [isOpen, setIsOpen] = useState(false);
    const [user, setUser] = useState("");
    

    
    const toggle = () => {
        setIsOpen(!isOpen);
    }
    
    const logout = () => {
        sessionStorage.clear();
        window.location.replace('#/register');
    }

    useEffect(()=>{
		if(sessionStorage.token){
			let user = JSON.parse(sessionStorage.user)
			setUser(user);
		}else{
			console.log(user);
		}
    }, [])
    
    return (
        <React.Fragment>
            <Navbar expand="md page-header text-custom border">
                      <div className="navbar-header">
                            <button type="button" className="navbar-toggler text-right" data-toggle="collapse">
                            <span className="sr-only">Toggle navigation</span>
                            <span className="icon-bar"></span>
                            <span className="icon-bar"></span>
                            <span className="icon-bar"></span>
                            </button>
                            
                   </div>
                <NavbarBrand href="/">
                     
                <img className="navbar logo" src={logo} alt="React.Space" height="114px" />
            
                </NavbarBrand>
                <NavbarToggler onClick={toggle} />
              
                <Collapse isOpen={isOpen} navbar>
                    <ul className="navbar-nav mr-auto">
                        <li className="nav-item active my-auto mx-2">
                            <Link to='/rooms'><strong>Spaces</strong></Link>
                        </li>          

                        <li className="nav-item active my-auto mx-2">
                            <Link to="/home"><strong>About us</strong></Link>
                        </li>
                        {user ? "" :
                            <li className="nav-item active my-auto mx-2">
                                <Link to="/login"><strong>Login</strong></Link>
                            </li>
                        }
                        {user ? "" :
                            <li className="nav-item active my-auto mx-2">
                                <Link to="/register"><strong>Register</strong></Link>
                            </li>
                        }
                        {user.isAdmin === "true" ?
							<li className="nav-item active my-auto mx-2">
                            <Link to="/showusers"><strong>Users</strong></Link>
                              </li>			
										: ""}
                        {user ? 
                        <li className="nav-item active my-auto mx-2">
                            <Link to="/showbookings"><strong>{user.firstName}'s Reservations</strong></Link>
                        </li>
                            : ""}
                        {user ? 
                        <li className="nav-item active my-auto mx-2">
                            <Link to="/login" onClick={logout}><strong>Logout</strong></Link>
                        </li>
                            : ""}
                        
                    </ul>

                     
                  </Collapse>
         
            </Navbar>
            
        </React.Fragment>
    )
}
export default NavigationBar;