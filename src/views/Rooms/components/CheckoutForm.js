import React from 'react';
import {CardElement, injectStripe} from 'react-stripe-elements';
import axios from 'axios';


const CheckoutForm = (props) => {
		// CommonJS
	const Swal = require('sweetalert2')

	const submit = async (e) => {
		let user = JSON.parse(sessionStorage.user);
		console.log(user.email);
		console.log(props.toPay);
		let { token } = await props.stripe.createToken({ name: "Name" })
		

		axios.post('https://dry-inlet-35094.herokuapp.com/charge',{
			email: user.email,
			amount: (props.toPay * 100)

		}).then(Swal.fire({
			title: 'Are you sure?',
			text: "Payment is nonrefundable.",
			icon: 'warning',
			showCancelButton: true,
			confirmButtonColor: '#3085d6',
			cancelButtonColor: '#d33',
			confirmButtonText: 'Yes, Pay Now!'
		}).then((result) => {
			if (result.value) {
				Swal.fire(
					'Success!',
					'Your booking has been booked.',
					'success'
				)
				
			}
		}));

		}

	return (
		<div className="checkout text-center">
			<h5> P{props.toPay}.00</h5>
			<h5>for {props.toDay} Days</h5>
        	<p className="py-3">Would you like to complete the payment?</p>
        	<CardElement py-2 />
        	<button 
        	className="btn btn-primary my-3 col-lg-12" 
			onClick={submit}>Pay Now</button>
      	</div>
	)
}

export default injectStripe(CheckoutForm);



