import React from 'react';
import {
	Modal,
	ModalHeader,
	ModalBody,
	Button
} from 'reactstrap';
import {FormInput} from '../../../globalcomponents'

const RoomForm = props => {
	return (
		<Modal
			isOpen={props.showForm}
			toggle={props.handleShowForm}
		>
			<ModalHeader
				toggle={props.handleShowForm}
				// style={{"backgroundColor":"salmon", "color": "white"}}
			>
			Add Space
			</ModalHeader>
			<ModalBody>
					
				<input
	                className = "form-control"
	                type = "file"
	                accept="image/*"
	                onChange = {(e)=>props.handleRoomImageChange(e)}
                    />
				<FormInput
					label={"Name:"}
					type={"text"}
					name={"name"}
					placeholder={"Enter the name of your space"}
					required={props.nameRequired}
					onChange={props.handleRoomNameChange}
				/>

				<label>Description:</label>
				<textarea
					className="form-control"
					type={"text"}
					name={"description"}
					placeholder={"Enter Your Space Description"}
					required={props.descriptionRequired}
					onChange={props.handleRoomDescriptionChange}
				/>
				<FormInput
					label={"Occupancy:"}
					name={"occupancy"}
					type={"number"}
					placeholder={"Enter your occupation"}
					required={props.occupancyRequired}
					onChange={props.handleRoomOccupancyChange}
				/>
				<FormInput
					label={"Price:"}
					type={"number"}
					name={"price"}
					placeholder={"Enter Price"}
					required={props.priceRequired}
					onChange={props.handleRoomPriceChange}
				/>
		
				<Button
					color="warning"
					onClick={props.handleSaveRoom}
					disabled={props.name!=="" && props.description !== "" && props.occupancy !== "" && props.occupancy > 0 ? false : true && props.price !== "" && props.price >0 ? false : true}
				>Add Your Space</Button>
			</ModalBody>
		</Modal>
	)
}

export default RoomForm;