import React, {useState, useEffect } from 'react';
import BookForm from './BookForm';


import {
	Button, 
	Col, 
	Card, 
	CardBody, 
	CardImg
} from 'reactstrap'
import {
FormInput
} from '../../../globalcomponents';

const RoomList = props => {

	const [showBookForm, setShowBookForm] = useState(false);
	const [isAdmin, setIsAdmin] = useState(false);

	const user = props.user;
	const room = props.room;
	console.log(room.image)

	useEffect(()=>{
		let user = JSON.parse(sessionStorage.user);
		setIsAdmin(user.isAdmin);
	},[]);


	const handleShowBookForm = () => {
		
			setShowBookForm(!showBookForm)
		
	}

	return (
		<React.Fragment>
		<Col lg={6}

		 className="mt-5">
		
			<Card className="card-custom text-center">		
				<CardImg top width="80%" height="300px" src={'https://dry-inlet-35094.herokuapp.com/' + room.image} alt="Room Image"
				 />
				 	{isAdmin === "true" ?
					<h4
					onClick={()=>props.handleNameEditInput(room._id)}
					className="pt-4"
					> {props.showName && props.editId === room._id
					? 
				<FormInput
					defaultValue={room.name}
					type={"text"}
					onBlur={(e)=>props.handleEditName(e, room._id)}
				/>
				: room.name}</h4> 
				   : <h4 className="pt-4 card-text"> {room.name}</h4>}
				

				{isAdmin === "true" ?
					<h6
					onClick={()=>props.handleDescriptionEditInput(room._id)}
					> {props.showDescription && props.editId === room._id
				? 
				<FormInput
					defaultValue={room.description}
					type={"text"}
					onBlur={(e)=>props.handleEditDescription(e, room._id)}
				/>
				: room.description}</h6>
				   : <h6 className="room-description"> {room.description}</h6>}

				   {isAdmin === "true" ?
				<h6
					onClick={()=>props.handleOccupancyEditInput(room._id)}

					>Maximum {props.showOccupancy && props.editId === room._id
						?
					 	<FormInput 
						defaultValue={room.occupancy}
						type={"number"}
						onBlur={(e)=>props.handleEditOccupancy(e, room._id)}
						/> 
					: room.occupancy
						} person/s</h6>
						  : <h6 className="card-text">Maximum {room.occupancy} person/s</h6>}
						{isAdmin === "true" ?  
					<h6
						onClick={()=>props.handlePriceEditInput(room._id)}
						>P{props.showPrice && props.editId === room._id
						?
					 	<FormInput 
						defaultValue={room.price}
						type={"number"}
						onBlur={(e)=>props.handleEditPrice(e, room._id)}
						/> 
					: room.price
						}.00 /day
					</h6>
					  : <h6 className="card-text">{room.price}.00/day </h6>}
						
				
							{isAdmin ==="true" ?
							<Button
							color="danger"
							onClick={()=>props.handleDeleteRoom(room._id)}
							className="col-lg-12 my-3"
							>
							Remove Room
							</Button>
							:
							<Button color="info" onClick={handleShowBookForm} 
							className="col-lg-12 my-3 button-text">Book</Button>
							}
			
						
					<BookForm
					showBookForm={showBookForm}
					handleShowBookForm={handleShowBookForm}
					room={room}
					user={user}
					
					/>
			</Card>
		</Col>

	

	
			

		

			
		</React.Fragment>
	)
}

export default RoomList;