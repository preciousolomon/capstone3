import React, {useEffect, useState} from 'react';
import {RoomList, RoomForm} from './components';
import {Loading} from '../../globalcomponents';
import {
	Button,
	Row,
	Container
} from 'reactstrap';
import moment from 'moment';
import axios from 'axios';
import {ToastContainer, toast} from 'react-toastify';
import {
	NavigationBar, Footer
} from "../Layouts"

const invalidPrice = () =>{
	toast.error("Invalid Price")
}

const invalidDescription = () =>{
	toast.error("Invalid Description")
}

const invalidName = () =>{
	toast.error("Invalid Name")
}

const invalidOccupancy = () =>{
	toast.error("Invalid Occupancy")
}





const Rooms = () => { 
	const [rooms, setRooms] = useState([]);
	const [isLoading, setIsLoading] = useState(true);
	const [showForm, setShowForm]= useState(false);
	const [roomId, setRoomId] = useState("");
	const [nameRequired, setNameRequired] = useState(true);
	const [name, setName] = useState("");
	const [description, setDescription] = useState("");
	const [descriptionRequired, setDescriptionRequired] = useState(true);
	const [occupancy, setOccupancy] = useState(0);
	const [occupancyRequired, setOccupancyRequired]=useState(true);
	const [price, setPrice]=useState(0);
	const [priceRequired, setPriceRequired]=useState(true);
	const [showPrice, setShowPrice] = useState(true);
	const [showOccupancy, setShowOccupancy] = useState(true);
	const [showDescription, setShowDescription] =useState(true);
	const [showName, setShowName] =useState(true);
	const [image, setImage] = useState({});
	const [imageRequired, setImageRequired]=useState(true);
	const [editId, setEditId] = useState("");
	const [user, setUser] = useState({});
	


	const handleRefresh = () => {
		setShowForm(false);
		
		setName("");
		setDescription("");
		setOccupancy(0);
		setPrice(0);
		setRoomId("");
		setImageRequired(true);
		setNameRequired(true);
		setDescriptionRequired(true);
		setOccupancyRequired(true);
		setPriceRequired(true);

	}

	useEffect(()=>{
		// fetch('')

		if(sessionStorage.token){
			let user = JSON.parse(sessionStorage.user);
			console.log(user.isAdmin)
			if(user.isAdmin){
				fetch('https://dry-inlet-35094.herokuapp.com/admin/showrooms'
				).then(response=>response.json()
				).then(data=>{
					setRooms(data);
					setIsLoading(false);
				});

				setUser(user);

			}else{
				fetch('https://dry-inlet-35094.herokuapp.com/admin/showrooms'
				).then(response=>response.json()
				).then(data=>{
					setRooms(data);
					setIsLoading(false);
				});

				setUser(user);
			}
		}else{
			window.location.replace('#/login')
		}
	},[])

	const handleShowForm = () =>{
		setShowForm(!showForm)
	}

	const handleRoomNameChange = (e) => {
		if(e.target.value===""){
			setNameRequired(true);
			setName("")
		}else{
			setNameRequired(false)
			setName(e.target.value);
		}
	}

	const handleRoomDescriptionChange = e => {
		if(e.target.value===""){
			setDescriptionRequired(true);
			setDescription("")
		}else{
			setDescriptionRequired(false)
			setDescription(e.target.value);
		}
	}

	const handleRoomOccupancyChange = e => {
		if(e.target.value===""){
			setOccupancyRequired(true);
			setOccupancy("")
		}else{
			setOccupancyRequired(false)
			setOccupancy(e.target.value);
		}
	}

	const handleRoomPriceChange = e => {
		if(e.target.value===""){
			setPriceRequired(true);
			setPrice(0)
		}else{
			setPriceRequired(false)
			setPrice(e.target.value);
		}
	}



	const handleRoomImageChange = e => {
		if (e.target.value === "") {
			setImageRequired(true);
			setImage(e.target.files.item(0))
		} else {
			setImageRequired(false)
			setImage(e.target.value)
		}
	}


	const handleSaveRoom = () => {
		let roomId = moment(new Date).format('x');

        const formData = new FormData();
        formData.append('name',name)
        formData.append('description',description)
        formData.append('image',image,image.name);
        formData.append('price', parseInt(price))
        formData.append('occupancy', parseInt(occupancy))
        formData.append('roomId',roomId)
        const config = {
            headers: {
                'content-type': 'multipart/form-data'
            }
        };
        try{
            axios.post('https://dry-inlet-35094.herokuapp.com/admin/addroom', formData,config).then(res=>{
                let newRooms = [...rooms];
                newRooms.push(res.data);
                setRooms(newRooms);
                handleRefresh();
            })
        }catch(e){
            console.log(e);
        }
    };


	const handleDeleteRoom = roomId => {
		axios({
			method: "DELETE",
			url: "https://dry-inlet-35094.herokuapp.com/admin/deleteroom/" + roomId
		}).then(res=>{
			let newRooms = rooms.filter(room=>room._id!==roomId);
			setRooms(newRooms)
		});
	}

	const handleEditPrice = (e,roomId) => {
		let price = e.target.value;
		// console.log(e.target.value)
		if(price <= 0){
			setShowPrice(true);
			invalidPrice();
		}else{
			axios({
				method:'PATCH',
				url: 'https://dry-inlet-35094.herokuapp.com/admin/updateprice/' + roomId,
				data: {
					price: price
				}
			}).then(res=>{
				let oldIndex;
				rooms.forEach((room,index)=>{
					if(room._id === roomId){
						oldIndex=index;
					}
				});

				let newRooms = [...rooms];
				newRooms.splice(oldIndex, 1, res.data);
				setRooms(newRooms);
				setShowPrice(true);
				setEditId("");
			})

		}
	}


	const handleEditOccupancy = (e,roomId) => {
		let occupancy = e.target.value;
		// console.log(e.target.value)
		if(occupancy <= 0){
			setShowOccupancy(true);
			invalidOccupancy();
		}else{
			axios({
				method:'PATCH',
				url: 'https://dry-inlet-35094.herokuapp.com/admin/updateoccupancy/' + roomId,
				data: {
					occupancy: occupancy
				}
			}).then(res=>{
				let oldIndex;
				rooms.forEach((room,index)=>{
					if(room._id === roomId){
						oldIndex=index;
					}
				});

				let newRooms = [...rooms];
				newRooms.splice(oldIndex, 1, res.data);
				setRooms(newRooms);
				setShowOccupancy(true);
				setEditId("");
			})
		}
	}

	const handleEditDescription = (e, roomId)=>{
		let description = e.target.value;
		if(description === ""){
			setShowDescription(true);
			invalidDescription();
		}else{
			axios({
				method: 'PATCH',
				url: "https://dry-inlet-35094.herokuapp.com/admin/updatedescription/" + roomId,
				data: {
					description: description
				}
			}).then(res=>{
				let oldIndex;
				rooms.forEach((room,index)=>{
					if(room._id === roomId){
						oldIndex=index;
					}
				});

				let newRooms = [...rooms];
				newRooms.splice(oldIndex, 1, res.data);
				setRooms(newRooms);
				setShowDescription(true);
				setEditId("");

			})
		}
	}




		const handleEditName = (e, roomId)=>{
		let name = e.target.value;
		if(name === ""){
			setShowName(true);
			invalidName();
		}else{
			axios({
				method: 'PATCH',
				url: "https://dry-inlet-35094.herokuapp.com/admin/updatename/" + roomId,
				data: {
				name: name
				}
			}).then(res=>{
				let oldIndex;
				rooms.forEach((room,index)=>{
					if(room._id === roomId){
						oldIndex=index;
					}
				});

				let newRooms = [...rooms];
				newRooms.splice(oldIndex, 1, res.data);
				setRooms(newRooms);
				setShowName(true);
				setEditId("");

			})
		}
	}


	const handlePriceEditInput = editId => {
		setEditId(editId)
		setShowPrice(true)
		setShowOccupancy(false)
		setShowDescription(false);
		setShowName(false);
	}

	const handleOccupancyEditInput = editId => {
		setEditId(editId);
		setShowPrice(false);
		setShowOccupancy(true);
		setShowDescription(false);
		setShowName(false);
	}

	const handleDescriptionEditInput = editId =>{
		setEditId(editId)
		setShowPrice(false);
		setShowOccupancy(false);
		setShowDescription(true);
		setShowName(false);

	}


	const handleNameEditInput = editId =>{
		setEditId(editId)
		setShowPrice(false);
		setShowOccupancy(false);
		setShowDescription(false);
		setShowName(true);
	}



	return (
		<React.Fragment>
		
		<ToastContainer />
		{isLoading ?
			<Loading />
		:
			<React.Fragment>
				<div className="page-head">
					<NavigationBar />
					<div className="page-header">
						<h1 className="text-center py-2 page-header">
						Space Rentals
						{user.isAdmin === "true" ?
							<Button
								color="success"
								onClick={handleShowForm}
							>Add New Spaces</Button>
						: ""}</h1>
					</div>
					<RoomForm
						showForm={showForm}
						handleShowForm={handleShowForm}

						handleRoomNameChange={handleRoomNameChange}
						nameRequired={nameRequired}

						handleRoomDescriptionChange={handleRoomDescriptionChange}
						descriptionRequired={descriptionRequired}

						handleRoomOccupancyChange={handleRoomOccupancyChange}
						occupancyRequired={occupancyRequired}

						handleRoomPriceChange={handleRoomPriceChange}
						priceRequired={priceRequired}

						handleRoomImageChange={handleRoomImageChange}
						imageRequired={imageRequired}

						image={image}
						name={name}
						description={description}
						occupancy={occupancy}
						price={price}

						handleSaveRoom={handleSaveRoom}
					/>			
				</div>
		
				<Container>
					<Row>
						
						{rooms.map(room=>(
							<RoomList 
								key={room._id}
								room={room}
								user={user}
								handleDeleteRoom={handleDeleteRoom}
								handleRoomImageChange={handleRoomImageChange}

								setShowPrice={setShowPrice}
								showPrice={showPrice}
								handleEditPrice={handleEditPrice}
								handlePriceEditInput={handlePriceEditInput}
								editId={editId}

								handleOccupancyEditInput={handleOccupancyEditInput}
								showOccupancy={showOccupancy}
								handleEditOccupancy={handleEditOccupancy}

								setShowDescription={setShowDescription}
								handleDescriptionEditInput={handleDescriptionEditInput}
								handleEditDescription={handleEditDescription}
								showDescription={showDescription}


								handleNameEditInput={handleNameEditInput}
								setShowName={setShowName}
								handleEditName={handleEditName}
								showName={showName}

								
							/>
						))}
				
						</Row>
					</Container>
				<Footer />	
			</React.Fragment>
		}
					
					
		</React.Fragment>
	)
}

export default Rooms;